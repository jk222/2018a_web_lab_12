package ictgradschool.web.lab12.ex3.Class;

public class Film {

    private String filmTitle;
    private String genreName;


    public String getFilmTitle() {
        return filmTitle;
    }

    public void setFilmTitle(String filmTitle) {
        this.filmTitle = filmTitle;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    @Override
    public String toString() {
        return "Film{" +
                "filmTitle='" + filmTitle + '\'' +
                ", genreName='" + genreName + '\'' +
                '}';
    }
}
