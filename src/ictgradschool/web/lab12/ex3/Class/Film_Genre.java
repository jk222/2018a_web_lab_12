package ictgradschool.web.lab12.ex3.Class;

public class Film_Genre {

    private String genre_Name;


    public String getGenre_Name() {
        return genre_Name;
    }

    public void setGenre_Name(String genre_Name) {
        this.genre_Name = genre_Name;
    }

    @Override
    public String toString() {
        return "Film_Genre{" +
                "genre_Name='" + genre_Name + '\'' +
                '}';
    }
}
