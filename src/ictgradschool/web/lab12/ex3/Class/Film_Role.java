package ictgradschool.web.lab12.ex3.Class;

public class Film_Role {
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    private String roleName;

    @Override
    public String toString() {
        return "Film_Role{" +
                "roleName='" + roleName + '\'' +
                '}';
    }
}
