package ictgradschool.web.lab12.ex3;

import ictgradschool.web.lab12.ex2.Keyboard;
import ictgradschool.web.lab12.ex3.Class.Film;
import ictgradschool.web.lab12.ex3.Class.Film_Actor;
import ictgradschool.web.lab12.ex3.Class.Film_Genre;
import ictgradschool.web.lab12.ex3.DAO.FilmDAO;
import ictgradschool.web.lab12.ex3.DAO.Film_ActorDAO;
import ictgradschool.web.lab12.ex3.DAO.Film_GenreDAO;

import java.util.List;

public class Exercise03 {
    public static void main(String[] args) {

        IntroMessage();

        String usersEntryOption = Keyboard.readInput();
        String usersEntryChoice;

        assert usersEntryOption != null;
        if (usersEntryOption.equals("1")) {

            List<Film_Actor> allActors = Film_ActorDAO.getAllActors(usersEntryOption);
            System.out.println("");
            System.out.println("Please enter the name of the actor you wish to get information about, or press" +
                    " enter to return to the previous menu");
            usersEntryChoice = Keyboard.readInput();





            if (usersEntryChoice.equals(allActors.get(0))) {


            } else {
                IntroMessage();
            }
        } else if (usersEntryOption.equals("2")) {
            List<Film> allFilms = FilmDAO.getAllFilms(usersEntryOption);
            System.out.println("");
            System.out.println("Please enter the name of the film you wish to get information about, or press" +
                    " enter to return to the previous menu");
            usersEntryChoice = Keyboard.readInput();
            if (usersEntryChoice.equals(allFilms.get(0))) {

            } else {
                IntroMessage();
            }
        } else if (usersEntryOption.equals("3")) {
            List<Film_Genre> allFilmGenres = Film_GenreDAO.getAllFilmGenres(usersEntryOption);
            System.out.println("");
            System.out.println("Please enter the name of the genre you wish to get information about, or press enter" +
                    " to return to the previous menu");
            usersEntryChoice = Keyboard.readInput();
            if (usersEntryChoice.equals(allFilmGenres.get(0))) {

            } else {
                IntroMessage();
            }
        } else if (usersEntryOption.equals("4")) {
            System.out.println("");
            System.exit(0);
        }
    }

    public  static void IntroMessage(){
        System.out.println("");
        System.out.println("Welcome to the Film Database");
        System.out.println("");
        System.out.println("Please select an option from the following: ");
        System.out.println("1. Information by Film Actor");
        System.out.println("2. Information by Film");
        System.out.println("3. Information by Genre");
        System.out.println("4. Exit");
        System.out.println("");
    }


}
