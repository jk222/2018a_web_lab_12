package ictgradschool.web.lab12.ex3.DAO;

import ictgradschool.web.lab12.ex3.Class.Film_Actor;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class Film_ActorDAO {
    static Properties dbProps = new Properties();

    static {
        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Film_Actor> getAllActors(String usersEntryOption) {
        List<Film_Actor> filmActorList = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement stmt = conn.prepareStatement("SELECT body, title, artid FROM lab12_articles WHERE title LIKE ?;")) {
                stmt.setString(1, "%" + usersEntryOption + "%");

                try (ResultSet r = stmt.executeQuery()) {
                    while (r.next()) {
                        Film_Actor actorName = new Film_Actor();

                        actorName.setFirstName(r.getString(1));
                        actorName.setLastName(r.getString(2));
                        filmActorList.add(actorName);
                    }
                }
            }
        } catch (SQLException e) {
            return new ArrayList<Film_Actor>();
        }

        return filmActorList;
    }

}
