package ictgradschool.web.lab12.ex3.DAO;

import ictgradschool.web.lab12.ex3.Class.Film;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class FilmDAO {
    static Properties dbProps = new Properties();

    static {
        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Film> getAllFilms(String usersEntryOption) {
        List<Film> filmList = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement stmt = conn.prepareStatement("SELECT body, title, artid FROM lab12_articles WHERE title LIKE ?;")) {
                stmt.setString(1, "%" + usersEntryOption + "%");

                try (ResultSet r = stmt.executeQuery()) {
                    while (r.next()) {
                        Film filmName = new Film();

                        filmName.setFilmTitle(r.getString(1));
                        filmName.setGenreName(r.getString(2));
                        filmList.add(filmName);
                    }
                }
            }
        } catch (SQLException e) {
            return new ArrayList<Film>();
        }

        return filmList;
    }
}
