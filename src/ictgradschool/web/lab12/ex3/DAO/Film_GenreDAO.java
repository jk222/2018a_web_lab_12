package ictgradschool.web.lab12.ex3.DAO;

import ictgradschool.web.lab12.ex3.Class.Film_Genre;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Film_GenreDAO {

    static Properties dbProps = new Properties();

    static {
        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Film_Genre> getAllFilmGenres(String usersEntryOption) {
        List<Film_Genre> filmGenreList = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement stmt = conn.prepareStatement("SELECT body, title, artid FROM lab12_articles WHERE title LIKE ?;")) {
                stmt.setString(1, "%" + usersEntryOption + "%");

                try (ResultSet r = stmt.executeQuery()) {
                    while (r.next()) {
                        Film_Genre filmGenre = new Film_Genre();

                        filmGenre.setGenre_Name(r.getString(1));
                        filmGenreList.add(filmGenre);
                    }
                }
            }
        } catch (SQLException e) {
            return new ArrayList<Film_Genre>();
        }

        return filmGenreList;
    }



}
