package ictgradschool.web.lab12.ex1;

import jdk.nashorn.internal.objects.annotations.Where;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.Console;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        boolean articleTitleMatch = false;
        do {


            System.out.println("");
            System.out.println("Enter a title of an article: ");

            String usersEntryTitle = Keyboard.readInput();

            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                try (PreparedStatement stmt = conn.prepareStatement("SELECT body FROM lab12_articles WHERE title LIKE ?;")) {
                    stmt.setString(1, "%" + usersEntryTitle + "%");

                    try (ResultSet r = stmt.executeQuery()) {
                        while (r.next()) {
                            String bodyArticle = r.getString(1);
                            System.out.println(bodyArticle);
                            System.out.println();
                            articleTitleMatch = true;
                        }
                    }

                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } while (!articleTitleMatch);


    }
}

