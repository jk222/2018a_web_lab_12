package ictgradschool.web.lab12.ex2;

import java.util.List;

public class Exercise02 {
    public static void main(String[] args)  {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        boolean articleTitleMatch = false;
        while (true) {
            System.out.println("");
            System.out.println("Enter a title of an article: ");

            String usersEntryTitle = Keyboard.readInput();

            List<Article> allArticles = ArticleDAO.getAllArticles(usersEntryTitle);

            if (allArticles.size() > 0) {
                for (Article article : allArticles) {
                    System.out.println(article);
                }

                // We were successful, exit
                break;
            } else {
                System.out.println("No article found with a title containing '" + usersEntryTitle + "'");
            }
        }
    }
}



