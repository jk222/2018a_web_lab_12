package ictgradschool.web.lab12.ex2;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class ArticleDAO {
    static Properties dbProps = new Properties();

    static {
        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Article> getAllArticles(String usersEntryTitle) {
        List<Article> articleList = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement stmt = conn.prepareStatement("SELECT body, title, artid FROM lab12_articles WHERE title LIKE ?;")) {
                stmt.setString(1,"%"+usersEntryTitle+"%");

                try (ResultSet r = stmt.executeQuery()) {
                    while (r.next()) {
                        Article article = new Article();

                        article.setBody(r.getString(1));
                        article.setTitle(r.getString(2));
                        article.setArtID(r.getInt(3));
                        articleList.add(article);
                    }
                }
            }
        } catch (SQLException e) {
            return new ArrayList<Article>();
        }

        return articleList;
    }
}





















