package ictgradschool.web.lab12.ex2;

public class Article {

    private int artID;
    private String title;
    private String body;

    public int getArtID() {
        return artID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setArtID(int artID) {
        this.artID = artID;
    }



    @Override
    public String toString() {
        return "Article :: [" + title + "] :: {" + body + "}";
    }
}
